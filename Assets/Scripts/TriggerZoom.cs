﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerZoom : MonoBehaviour
{
    Camera[] cams;
    public bool zooming;

    public float zoomRate = 10;
    public float zoomVal = 0;

    // Start is called before the first frame update
    void Start()
    {
        cams = GetComponentsInChildren<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (zooming)
        {
            if (zoomVal <= 3)
            {
                zoomVal = zoomVal + (Time.deltaTime * zoomRate);
            }
        }
        else
        {
            if (zoomVal >= 0)
            {
                zoomVal = zoomVal - (Time.deltaTime * zoomRate);
            }
        }

        foreach (Camera c in cams)
        {
            c.transform.parent.parent.localPosition = new Vector3(c.transform.parent.parent.localPosition.x, c.transform.parent.parent.localPosition.y, zoomVal);
        }
    }

    public void ZoomCamera(bool zoom)
    {
        if (zoom)
        {
            zooming = true;
        }
        else
        {
            zooming = false;
        }
    }
}
