﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDying : MonoBehaviour
{
    AudioSource audSource;
    Transform fire;
    Transform chlorine;
    Animator gettingHurt;
    int buildIndex;

    private void Start()
    {
        audSource = GameObject.Find("HeartBeatAudio").GetComponent<AudioSource>();
        fire = GameObject.Find("FlamesParticleEffect").transform;
        chlorine = GameObject.Find("Zone (Close)").transform;
        gettingHurt = GameObject.Find("GettingHurt").GetComponent<Animator>();
        buildIndex = SceneManager.GetActiveScene().buildIndex;
    }

    private void Update()
    {
        if (buildIndex == 2)
        {
            if (chlorine != null && Vector3.Distance(chlorine.position, transform.position) <= 5 && !GameObject.Find("Player Suit").transform.GetChild(0).gameObject.activeSelf)
            {
                if (!gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", true);

                if (!audSource.isPlaying)
                    audSource.Play();
            }
            else if (fire != null && Vector3.Distance(fire.position, transform.position) <= 5)
            {
                if (!gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", true);

                if (!audSource.isPlaying)
                    audSource.Play();
            }
            else if (chlorine != null && Vector3.Distance(chlorine.position, transform.position) > 5 && !GameObject.Find("Player Suit").transform.GetChild(0).gameObject.activeSelf)
            {
                if (gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", false);

                if (audSource.isPlaying)
                    audSource.Stop();
            }
            else if (fire != null && Vector3.Distance(fire.position, transform.position) > 5)
            {
                if (gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", false);

                if (audSource.isPlaying)
                    audSource.Stop();
            }
        }
        else if (buildIndex == 4)
        {
            if (fire != null && Vector3.Distance(fire.position, transform.position) <= 12)
            {
                if (!gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", true);

                if (!audSource.isPlaying)
                    audSource.Play();
            }
            else if (fire != null && Vector3.Distance(fire.position, transform.position) > 12)
            {
                if (gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", false);

                if (audSource.isPlaying)
                    audSource.Stop();
            }
        }
        else
        {
            if (fire != null && Vector3.Distance(fire.position, transform.position) <= 5)
            {
                if (!gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", true);

                if (!audSource.isPlaying)
                    audSource.Play();
            }
            else if (fire != null && Vector3.Distance(fire.position, transform.position) > 5)
            {
                if (gettingHurt.GetBool("Hurt"))
                    gettingHurt.SetBool("Hurt", false);

                if (audSource.isPlaying)
                    audSource.Stop();
            }
        }
    }

    public void StopHurting()
    {
        if (gettingHurt.GetBool("Hurt"))
            gettingHurt.SetBool("Hurt", false);

        if (audSource.isPlaying)
            audSource.Stop();
    }
}
