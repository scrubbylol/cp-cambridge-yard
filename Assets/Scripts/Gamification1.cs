﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Gamification1 : MonoBehaviour
{
    public AudioClip completeClip;
    public AudioClip sandClip;
    public AudioClip wrenchClip;
    public AudioClip hollowClip;
    public string[] checklist;
    public Material solidConeMaterial;
    public int leaksSpilled;
    public int coneCount;

    TMP_Text checklistText;
    AudioSource audSource;

    BoxCollider playerCollider;
    GameObject shovelInHand;
    GameObject fillGauge;
    GameObject[] leaks;

    public int beepCount;
    public int task = 1;

    bool fixing;
    float fixingTimer;

    // Start is called before the first frame update
    void Start()
    {
        fillGauge = GameObject.Find("FillGauge");
        fillGauge.SetActive(false);

        audSource = GetComponent<AudioSource>();

        checklistText = GameObject.Find("Checklist").GetComponent<TMP_Text>();
        UpdateChecklist();

        playerCollider = GetComponent<BoxCollider>();

        leaks = GameObject.FindGameObjectsWithTag("LeakParticle");
    }

    // Update is called once per frame
    void Update()
    {
        // Slow/stop releases
        if (task == 5)
        {
            if (Vector3.Distance(leaks[0].transform.position, transform.position) <= 2 && leaks[0].transform.GetChild(0).gameObject.activeSelf)
            {
                if (!fillGauge.activeSelf)
                    fillGauge.SetActive(true);

                if (!audSource.isPlaying)
                {
                    audSource.clip = wrenchClip;
                    audSource.loop = true;
                    audSource.Play();
                }

                fixingTimer += Time.deltaTime;
                fillGauge.GetComponent<Slider>().value = fixingTimer;

                // Finished
                if (fixingTimer >= 10)
                {
                    fixingTimer = 0;
                    Destroy(leaks[0].transform.parent.GetComponentInChildren<ParticleSystem>().gameObject);
                    Destroy(leaks[0]);

                    if (leaks[1] != null)
                    {
                        leaks[1].transform.GetChild(0).gameObject.SetActive(true);
                        leaks[0] = leaks[1];
                        leaks[1] = null;
                    }
                    else
                    {
                        checklist[4] = "<s>" + checklist[4].Split('>')[1].Split('<')[0] + "</s>";
                        fillGauge.SetActive(false);
                        audSource.loop = false;
                        audSource.Stop();
                        audSource.PlayOneShot(completeClip);
                        task = 6;
                        UpdateChecklist();
                    }
                }
            }
            else if (Vector3.Distance(leaks[0].transform.position, transform.position) > 2)
            {
                if (fillGauge.activeSelf)
                    fillGauge.SetActive(false);

                if (audSource.clip != null)
                {
                    if (audSource.isPlaying && audSource.clip.name.Equals(wrenchClip.name))
                    {
                        audSource.Stop();
                        audSource.loop = false;
                    }
                }
            }
        }
    }

    void UpdateChecklist()
    {
        checklistText.text = "Checklist:";

        foreach (string s in checklist)
        {
            checklistText.text = checklistText.text + "\n" + s;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Get paperwork from crew
        if (task == 1 && other.name.Equals("Crew"))
        {
            checklist[0] = "<s>" + checklist[0].Split('>')[1].Split('<')[0] + "</s>";
            checklist[1] = "<color=red>" + checklist[1] + "</color>";

            GameObject.Find("ClosedArea").transform.GetChild(0).gameObject.SetActive(true);
            other.transform.GetChild(0).gameObject.SetActive(false);
            audSource.PlayOneShot(completeClip);
            task = 2;
            UpdateChecklist();
        }

        // Deploy cones
        if (task == 2 && other.name.Equals("Cone"))
        {
            if (other.GetComponent<BoxCollider>().isTrigger)
            {
                audSource.PlayOneShot(hollowClip);
                other.transform.GetChild(0).gameObject.SetActive(false);
                other.GetComponent<MeshRenderer>().material = solidConeMaterial;
                other.GetComponent<BoxCollider>().isTrigger = false;
                other.GetComponent<Rigidbody>().isKinematic = false;
                coneCount -= 1;
            }

            if (coneCount == 0)
            {
                checklist[1] = "<s>" + checklist[1].Split('>')[1].Split('<')[0] + "</s>";
                checklist[2] = "<color=red>" + checklist[2] + "</color>";

                audSource.PlayOneShot(completeClip);
                beepCount = 0;
                task = 3;
                UpdateChecklist();
            }
        }

        // Get airmonitor readings
        if (task == 3 && GetComponent<PlayerManager>().beeping)
        {
            if (beepCount >= 2)
            {
                checklist[2] = "<s>" + checklist[2].Split('>')[1].Split('<')[0] + "</s>";
                checklist[3] = "<color=red>" + checklist[3] + "</color>";

                GameObject.Find("Shovel").transform.GetChild(0).gameObject.SetActive(true);
                audSource.PlayOneShot(completeClip);
                task = 4;
                UpdateChecklist();
            }
        }

        // Put sand on spills - grab shovel
        if (task == 4 && other.name.Equals("Shovel"))
        {
            other.gameObject.SetActive(false);
            shovelInHand = GameObject.Find("RightHand").transform.GetChild(1).gameObject;
            shovelInHand.SetActive(true);
            GameObject.Find("RightHand").transform.GetChild(GameObject.Find("RightHand").transform.childCount - 1).gameObject.SetActive(false);
            audSource.PlayOneShot(sandClip, 0.65f);
        }

        // Put sand on spills - grab more sand
        if (task == 4 && other.name.Equals("Sand Pile") && shovelInHand != null)
        {
            if (!shovelInHand.transform.GetChild(0).GetComponent<MeshRenderer>().enabled)
            {
                GameObject.Find("Sand Pile").transform.GetChild(0).gameObject.SetActive(false);
                shovelInHand.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                audSource.PlayOneShot(sandClip, 0.65f);
            }
        }

        // Put sand on spills - put on leak
        if (task == 4 && other.tag.Equals("Leak") && shovelInHand != null)
        {
            if (shovelInHand.transform.GetChild(0).GetComponent<MeshRenderer>().enabled)
            {
                GameObject.Find("Sand Pile").transform.GetChild(0).gameObject.SetActive(true);

                int countSand = 2;
                for (int i = 0; i < other.transform.childCount; i ++)
                {
                    if (!other.transform.GetChild(i).gameObject.activeSelf && countSand > 0)
                    {
                        shovelInHand.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                        other.transform.GetChild(i).gameObject.SetActive(true);
                        countSand -= 1;
                    }
                }

                bool allActive = true;

                // Check if all sand is placed
                for (int j = 0; j < other.transform.childCount; j++)
                {
                    if (!other.transform.GetChild(j).gameObject.activeSelf)
                    {
                        allActive = false;
                    }
                }

                // Check if leak is contained
                if (allActive)
                {
                    other.GetComponent<BoxCollider>().enabled = false;
                    leaksSpilled -= 1;

                    // Check if this was the last leak spilled
                    if (leaksSpilled == 0)
                    {
                        GameObject.Find("Sand Pile").transform.GetChild(0).gameObject.SetActive(false);
                        shovelInHand.SetActive(false);
                        GameObject.Find("RightHand").transform.GetChild(2).gameObject.SetActive(true);
                        checklist[3] = "<s>" + checklist[3].Split('>')[1].Split('<')[0] + "</s>";
                        checklist[4] = "<color=red>" + checklist[4] + "</color>";

                        leaks[0].transform.GetChild(0).gameObject.SetActive(true);

                        audSource.PlayOneShot(completeClip);
                        task = 5;
                        UpdateChecklist();
                    }
                }
            }
        }
    }
}
