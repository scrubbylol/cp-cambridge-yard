﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrewarmParticles : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ParticleSystem.MainModule p = GetComponent<ParticleSystem>().main;
        p.prewarm = true;

        GetComponent<ParticleSystem>().Stop();
        GetComponent<ParticleSystem>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
