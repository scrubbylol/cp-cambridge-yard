﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public bool inZone = true;

    public GameObject airmonitor;
    private GameObject[] airMonitoringZones;

    private Text[] airMonitorText;

    public AudioSource airMonitorSource;
    public AudioClip beepSound;

    public Material airMonitorMaterialOriginal;
    public Material airMonitorMaterialBeep1;
    public Material airMonitorMaterialBeep2;

    bool holdingAirMonitor;
    float beepTimer;

    public bool beeping;

    // Start is called before the first frame update
    void Start()
    {
        airMonitorText = airmonitor.transform.GetChild(0).GetChild(1).GetComponentsInChildren<Text>();
        airMonitoringZones = GameObject.FindGameObjectsWithTag("Zone");
    }

    private void Update()
    {
        if (airmonitor.activeSelf)
        {
            if (!CheckIfInZone())
            {
                airMonitorText[0].text = "20.9";
                airMonitorText[1].text = "0";
                airMonitorText[2].text = "0";
                airMonitorText[3].text = "0";
                airMonitorText[4].text = "0";
            }

            // x.1 - Beeps airmonitor over 25 for VOCs
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                if (float.Parse(airMonitorText[4].text) >= 20.0f)
                    Beep();
            }
            // x.2 - Beeps airmonitor over 15 for CL2
            else if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                if (float.Parse(airMonitorText[2].text) >= 15.0f)
                    Beep();
            }
            // x.3 - Beeps airmonitor over 20 for VOCs
            else if (SceneManager.GetActiveScene().buildIndex == 3)
            {
                if (float.Parse(airMonitorText[4].text) >= 20.0f)
                    Beep();
            }
            // x.4 - Beeps airmonitor over 20 for VOCs
            else if (SceneManager.GetActiveScene().buildIndex == 4)
            {
                if (float.Parse(airMonitorText[4].text) >= 20.0f)
                    Beep();
            }
            // x.5 - Beeps airmonitor over 20 for VOCs
            else if (SceneManager.GetActiveScene().buildIndex == 5)
            {
                if (float.Parse(airMonitorText[4].text) >= 20.0f)
                    Beep();
            }
        }
    }

    void Beep()
    {
        beepTimer += Time.deltaTime;
        if (beepTimer > 1.0f)
        {
            StartCoroutine(Play3Beeps());
            beepTimer = 0;
        }
    }

    public bool CheckIfInZone()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            foreach (GameObject z in airMonitoringZones)
            {
                if (GameObject.FindGameObjectWithTag("Player").GetComponent<BoxCollider>().bounds.Intersects(z.GetComponent<BoxCollider>().bounds))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool CheckZonesUpdating()
    {
        foreach (GameObject z in airMonitoringZones)
        {
            if (z.GetComponent<AirMonitoring>().updating)
            {
                return true;
            }
        }
        return false;
    }

    IEnumerator Play3Beeps()
    {
        beeping = true;
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.PlayOneShot(beepSound);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialBeep1;
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.PlayOneShot(beepSound);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialBeep2;
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.PlayOneShot(beepSound);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialBeep1;
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialOriginal;

        if (SceneManager.GetActiveScene().buildIndex == 1)
            GetComponent<Gamification1>().beepCount += 1;
        else if (SceneManager.GetActiveScene().buildIndex == 2)
            GetComponent<Gamification2>().beepCount += 1;
        else if (SceneManager.GetActiveScene().buildIndex == 3)
            GetComponent<Gamification3>().beepCount += 1;
        else if (SceneManager.GetActiveScene().buildIndex == 4)
            GetComponent<Gamification4>().beepCount += 1;
        else if (SceneManager.GetActiveScene().buildIndex == 5)
            GetComponent<Gamification5>().beepCount += 1;

        beeping = false;
    }

    public void ToggleHoldingAirMonitor(bool hold)
    {
        holdingAirMonitor = hold;
    }
}
