﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderClimb : MonoBehaviour
{
    bool canClimb;
    public float climbAmount = 1;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ladder"))
        {
            canClimb = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ladder"))
        {
            canClimb = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (canClimb)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(new Vector3(0, climbAmount, 0) * Time.deltaTime * 1);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(new Vector3(0, -climbAmount, 0) * Time.deltaTime * 1);
            }
        }
    }
}
