﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Gamification4 : MonoBehaviour
{
    public AudioClip completeClip;
    public AudioClip sandClip;
    public AudioClip wrenchClip;
    public AudioClip hollowClip;
    public AudioClip suitClip;
    public string[] checklist;
    public Material solidConeMaterial;
    public int leaksSpilled;
    public int coneCount;

    TMP_Text checklistText;
    AudioSource audSource;

    BoxCollider playerCollider;
    GameObject shovelInHand;

    [HideInInspector]
    public GameObject hoseInHand;
    GameObject fillGauge;
    GameObject[] leaks;

    public int beepCount;
    public int task = 1;

    bool fixing;
    float fixingTimer;

    // Start is called before the first frame update
    void Start()
    {
        fillGauge = GameObject.Find("FillGauge");
        fillGauge.SetActive(false);

        audSource = GetComponent<AudioSource>();

        checklistText = GameObject.Find("Checklist").GetComponent<TMP_Text>();
        UpdateChecklist();

        playerCollider = GetComponent<BoxCollider>();

        leaks = GameObject.FindGameObjectsWithTag("LeakParticle");
    }

    void UpdateChecklist()
    {
        checklistText.text = "Checklist:";

        foreach (string s in checklist)
        {
            checklistText.text = checklistText.text + "\n" + s;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Get paperwork from crew
        if (task == 1 && other.name.Equals("Crew"))
        {
            checklist[0] = "<s>" + checklist[0].Split('>')[1].Split('<')[0] + "</s>";
            checklist[1] = "<color=red>" + checklist[1] + "</color>";

            GameObject.Find("ClosedArea").transform.GetChild(0).gameObject.SetActive(true);
            other.transform.GetChild(0).gameObject.SetActive(false);
            audSource.PlayOneShot(completeClip);
            task = 2;
            UpdateChecklist();
        }

        // Deploy cones
        if (task == 2 && other.name.Equals("Cone"))
        {
            if (other.GetComponent<BoxCollider>().isTrigger)
            {
                audSource.PlayOneShot(hollowClip);
                other.transform.GetChild(0).gameObject.SetActive(false);
                other.GetComponent<MeshRenderer>().material = solidConeMaterial;
                other.GetComponent<BoxCollider>().isTrigger = false;
                other.GetComponent<Rigidbody>().isKinematic = false;
                coneCount -= 1;
            }

            if (coneCount == 0)
            {
                checklist[1] = "<s>" + checklist[1].Split('>')[1].Split('<')[0] + "</s>";
                checklist[2] = "<color=red>" + checklist[2] + "</color>";

                audSource.PlayOneShot(completeClip);
                beepCount = 0;
                task = 3;
                UpdateChecklist();
            }
        }

        // Get airmonitor readings
        if (task == 3 && GetComponent<PlayerManager>().beeping)
        {
            if (beepCount >= 2)
            {
                checklist[2] = "<s>" + checklist[2].Split('>')[1].Split('<')[0] + "</s>";
                checklist[3] = "<color=red>" + checklist[3] + "</color>";

                GameObject.Find("Fire Truck").transform.GetChild(0).gameObject.SetActive(true);
                GameObject.Find("FireCollider").GetComponent<SphereCollider>().enabled = true;
                GameObject.Find("Hole").transform.GetChild(0).gameObject.SetActive(true);
                audSource.PlayOneShot(completeClip);
                task = 4;
                UpdateChecklist();
            }
        }

        // Grab hose and put out fire
        if (task == 4 && other.name.Equals("GetHose"))
        {
            other.gameObject.SetActive(false);
            GameObject.Find("Fire Truck").transform.GetChild(0).gameObject.SetActive(false);
            hoseInHand = GameObject.Find("RightHand").transform.GetChild(1).gameObject;
            hoseInHand.SetActive(true);
            GameObject.Find("RightHand").transform.GetChild(GameObject.Find("RightHand").transform.childCount - 1).gameObject.SetActive(false);
        }
    }

    public void UpdateTask(int newTask)
    {
        checklist[newTask-2] = "<s>" + checklist[newTask-2].Split('>')[1].Split('<')[0] + "</s>";

        audSource.PlayOneShot(completeClip);
        task = newTask;
        UpdateChecklist();
    }
}
