﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaterCollision : MonoBehaviour
{
    Gamification2 game2;
    Gamification3 game3;
    Gamification4 game4;
    Gamification5 game5;
    int buildIndex;

    // Start is called before the first frame update
    void Start()
    {
        buildIndex = SceneManager.GetActiveScene().buildIndex;

        if (buildIndex == 2)
            game2 = FindObjectOfType<Gamification2>();
        else if (buildIndex == 3)
            game3 = FindObjectOfType<Gamification3>();
        else if (buildIndex == 4)
            game4 = FindObjectOfType<Gamification4>();
        else if (buildIndex == 5)
            game5 = FindObjectOfType<Gamification5>();
    }

    private void Update()
    {
        // Increase / decrease water pressure
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            GetComponent<ParticleSystem>().gravityModifier -= 0.001f;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            GetComponent<ParticleSystem>().gravityModifier += 0.001f;
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (buildIndex == 2)
        {
            if (game2.task == 4 && other.name.Equals("FireCollider"))
            {
                GameObject.Find("Running Water").GetComponent<ParticleSystem>().Play();

                if (!GameObject.Find("Hole").transform.GetChild(1).gameObject.activeSelf)
                    GameObject.Find("Hole").transform.GetChild(1).gameObject.SetActive(true);

                Transform fire = other.transform.parent;
                if (fire.localScale.z >= 0.5f)
                {
                    fire.localScale = new Vector3(fire.localScale.x - 0.004f, fire.localScale.y - 0.004f, fire.localScale.z - 0.004f);
                }
                else
                {
                    fire.GetComponent<AudioSource>().volume = 0.25f;
                    GameObject.Find("Shovel").transform.GetChild(0).gameObject.SetActive(true);
                    game2.hoseInHand.SetActive(false);
                    game2.UpdateTask(5);
                }
            }
        }
        else if (buildIndex == 3)
        {
            if (game3.task == 4 && other.name.Equals("FireCollider"))
            {
                GameObject.Find("Running Water").GetComponent<ParticleSystem>().Play();

                if (!GameObject.Find("Hole").transform.GetChild(1).gameObject.activeSelf)
                    GameObject.Find("Hole").transform.GetChild(1).gameObject.SetActive(true);

                Transform fire = other.transform.parent;
                if (fire.localScale.z >= 0.5f)
                {
                    fire.localScale = new Vector3(fire.localScale.x - 0.005f, fire.localScale.y - 0.005f, fire.localScale.z - 0.005f);
                }
                else
                {
                    fire.GetComponent<AudioSource>().volume = 0.25f;
                    GameObject.Find("Shovel").transform.GetChild(0).gameObject.SetActive(true);
                    game3.hoseInHand.SetActive(false);
                    game3.UpdateTask(5);
                }
            }
        }
        else if (buildIndex == 4)
        {
            if (game4.task == 4 && other.name.Equals("FireCollider"))
            {
                GameObject.Find("Running Water").GetComponent<ParticleSystem>().Play();

                if (!GameObject.Find("Hole").transform.GetChild(1).gameObject.activeSelf)
                    GameObject.Find("Hole").transform.GetChild(1).gameObject.SetActive(true);

                Transform fire = other.transform.parent;
                if (fire.localScale.z >= 0.35f)
                {
                    fire.localScale = new Vector3(fire.localScale.x - 0.004f, fire.localScale.y - 0.004f, fire.localScale.z - 0.004f);
                }
                else
                {
                    Destroy(fire.gameObject);
                    FindObjectOfType<PlayerDying>().StopHurting();
                    other.transform.parent.GetComponent<AudioSource>().Stop();
                    game4.hoseInHand.SetActive(false);
                    game4.UpdateTask(5);
                }
            }
        }
        else if (buildIndex == 5)
        {
            if (game5.task == 4 && other.name.Equals("FireCollider"))
            {
                GameObject.Find("Running Water").GetComponent<ParticleSystem>().Play();

                if (!GameObject.Find("Hole").transform.GetChild(1).gameObject.activeSelf)
                    GameObject.Find("Hole").transform.GetChild(1).gameObject.SetActive(true);

                Transform fire = other.transform.parent;
                if (fire.localScale.z >= 0.5f)
                {
                    fire.localScale = new Vector3(fire.localScale.x - 0.005f, fire.localScale.y - 0.005f, fire.localScale.z - 0.005f);
                }
                else
                {
                    Destroy(fire.gameObject);
                    FindObjectOfType<PlayerDying>().StopHurting();
                    other.transform.parent.GetComponent<AudioSource>().Stop();
                    GameObject.Find("Shovel").transform.GetChild(0).gameObject.SetActive(true);
                    game5.hoseInHand.SetActive(false);
                    game5.UpdateTask(5);
                }
            }
        }
    }
}
