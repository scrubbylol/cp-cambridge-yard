﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenScenario : MonoBehaviour
{
    public void Open(int scenario)
    {
        SceneManager.LoadScene(scenario);
    }
}
